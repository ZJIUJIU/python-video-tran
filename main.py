import sys

from PySide2.QtWidgets import QWidget, QApplication, QFrame
from PySide2.QtCore import Qt, QFile, QTextStream
from PySide2.QtUiTools import QUiLoader
from ui.ListMenuFrame import Ui_ListMenuFrame
from ui.MainWindow import MainWindow


def loadQss(widget, file_path):
    qss_file = QFile(file_path)
    if qss_file.exists():
        qss_file.open(QFile.ReadOnly | QFile.Text)
        stream = QTextStream(qss_file)
        style_sheet = stream.readAll()
        qss_file.close()
        # 设置窗口样式表
        widget.setStyleSheet(style_sheet)


def loadUi(widget, file_path):
    qss_file = QFile(file_path)
    if qss_file.exists():
        qss_file.open(QFile.ReadOnly)
        widget.ui = QUiLoader().load(qss_file)


class ListMenuFrame(QFrame, Ui_ListMenuFrame):
    def __init__(self, parent=None):
        super(ListMenuFrame, self).__init__(parent)
        self.setupUi(self)

    def initWidget(self):
        pass





# 按装订区域中的绿色按钮以运行脚本。
if __name__ == '__main__':
    app = QApplication()
    loadQss(app, "qss/style.qss")

    # You codes
    mainWindow = MainWindow()
    mainWindow.show()

    app.installEventFilter(mainWindow)

    sys.exit(app.exec_())
