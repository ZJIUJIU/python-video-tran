# -*- coding: utf-8 -*-
import sys

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from .AddDialog import AddDialog
from .UI_MainWindow import Ui_MainWindow


class MainWindow(QWidget, Ui_MainWindow):
    def __init__(self, title="Python:视频语音转化", parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(title)
        # 初始化
        self.initWindow()

    def initWindow(self):
        # 无边框
        self.setWindowFlags(Qt.FramelessWindowHint)
        # 关闭事件
        self.pushButton_close.clicked.connect(self.close)
        self.pushButton_add.clicked.connect(self.addHandler)

    def close(self):
        sys.exit(0)

    def addHandler(self):
        # 新增按钮,打开新增dialog
        add_dialog = AddDialog()
        add_dialog.setWindowTitle("新增转换")
        add_dialog.exec_()
        # background color rgb(52, 52, 52)
