# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ListMenuFrame.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_ListMenuFrame(object):
    def setupUi(self, ListMenuFrame):
        if not ListMenuFrame.objectName():
            ListMenuFrame.setObjectName(u"ListMenuFrame")
        ListMenuFrame.resize(212, 739)
        self.gridLayout = QGridLayout(ListMenuFrame)
        self.gridLayout.setObjectName(u"gridLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(ListMenuFrame)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(0, 40))
        self.label.setMaximumSize(QSize(16777215, 40))
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)

        self.verticalLayout.addWidget(self.label)

        self.pushButton_submenu_wait = QPushButton(ListMenuFrame)
        self.pushButton_submenu_wait.setObjectName(u"pushButton_submenu_wait")
        sizePolicy.setHeightForWidth(self.pushButton_submenu_wait.sizePolicy().hasHeightForWidth())
        self.pushButton_submenu_wait.setSizePolicy(sizePolicy)
        self.pushButton_submenu_wait.setMinimumSize(QSize(0, 40))
        self.pushButton_submenu_wait.setMaximumSize(QSize(16777215, 40))

        self.verticalLayout.addWidget(self.pushButton_submenu_wait)

        self.pushButton__submenu_trans = QPushButton(ListMenuFrame)
        self.pushButton__submenu_trans.setObjectName(u"pushButton__submenu_trans")
        sizePolicy.setHeightForWidth(self.pushButton__submenu_trans.sizePolicy().hasHeightForWidth())
        self.pushButton__submenu_trans.setSizePolicy(sizePolicy)
        self.pushButton__submenu_trans.setMinimumSize(QSize(0, 40))
        self.pushButton__submenu_trans.setMaximumSize(QSize(16777215, 40))

        self.verticalLayout.addWidget(self.pushButton__submenu_trans)

        self.pushButton__submenu_end = QPushButton(ListMenuFrame)
        self.pushButton__submenu_end.setObjectName(u"pushButton__submenu_end")
        sizePolicy.setHeightForWidth(self.pushButton__submenu_end.sizePolicy().hasHeightForWidth())
        self.pushButton__submenu_end.setSizePolicy(sizePolicy)
        self.pushButton__submenu_end.setMinimumSize(QSize(0, 40))
        self.pushButton__submenu_end.setMaximumSize(QSize(16777215, 40))

        self.verticalLayout.addWidget(self.pushButton__submenu_end)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.gridLayout.addLayout(self.verticalLayout_2, 0, 0, 1, 1)


        self.retranslateUi(ListMenuFrame)

        QMetaObject.connectSlotsByName(ListMenuFrame)
    # setupUi

    def retranslateUi(self, ListMenuFrame):
        ListMenuFrame.setWindowTitle(QCoreApplication.translate("ListMenuFrame", u"Frame", None))
        self.label.setText(QCoreApplication.translate("ListMenuFrame", u"\u4efb\u52a1\u5217\u8868", None))
        self.pushButton_submenu_wait.setText(QCoreApplication.translate("ListMenuFrame", u"\u7b49\u5f85\u4e2d", None))
        self.pushButton__submenu_trans.setText(QCoreApplication.translate("ListMenuFrame", u"\u8f6c\u6362\u4e2d", None))
        self.pushButton__submenu_end.setText(QCoreApplication.translate("ListMenuFrame", u"\u5df2\u5b8c\u6210", None))
    # retranslateUi

