# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'AddDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_AddDialog(object):
    def setupUi(self, AddDialog):
        if not AddDialog.objectName():
            AddDialog.setObjectName(u"AddDialog")
        AddDialog.resize(837, 340)
        self.gridLayout_2 = QGridLayout(AddDialog)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(AddDialog)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.lineEdit = QLineEdit(AddDialog)
        self.lineEdit.setObjectName(u"lineEdit")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.lineEdit)


        self.horizontalLayout_6.addLayout(self.formLayout)

        self.pushButton_3 = QPushButton(AddDialog)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout_6.addWidget(self.pushButton_3)


        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.formLayout_2 = QFormLayout()
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.label_2 = QLabel(AddDialog)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_2)

        self.lineEdit_2 = QLineEdit(AddDialog)
        self.lineEdit_2.setObjectName(u"lineEdit_2")

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.lineEdit_2)


        self.horizontalLayout_7.addLayout(self.formLayout_2)

        self.pushButton_4 = QPushButton(AddDialog)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout_7.addWidget(self.pushButton_4)


        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.formLayout_3 = QFormLayout()
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.label_3 = QLabel(AddDialog)
        self.label_3.setObjectName(u"label_3")

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole, self.label_3)

        self.comboBox = QComboBox(AddDialog)
        self.comboBox.setObjectName(u"comboBox")

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole, self.comboBox)


        self.horizontalLayout.addLayout(self.formLayout_3)

        self.formLayout_4 = QFormLayout()
        self.formLayout_4.setObjectName(u"formLayout_4")
        self.label_4 = QLabel(AddDialog)
        self.label_4.setObjectName(u"label_4")

        self.formLayout_4.setWidget(0, QFormLayout.LabelRole, self.label_4)

        self.lineEdit_3 = QLineEdit(AddDialog)
        self.lineEdit_3.setObjectName(u"lineEdit_3")

        self.formLayout_4.setWidget(0, QFormLayout.FieldRole, self.lineEdit_3)


        self.horizontalLayout.addLayout(self.formLayout_4)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.formLayout_5 = QFormLayout()
        self.formLayout_5.setObjectName(u"formLayout_5")
        self.label_5 = QLabel(AddDialog)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_5.setWidget(0, QFormLayout.LabelRole, self.label_5)

        self.comboBox_2 = QComboBox(AddDialog)
        self.comboBox_2.setObjectName(u"comboBox_2")

        self.formLayout_5.setWidget(0, QFormLayout.FieldRole, self.comboBox_2)


        self.horizontalLayout_2.addLayout(self.formLayout_5)

        self.formLayout_6 = QFormLayout()
        self.formLayout_6.setObjectName(u"formLayout_6")
        self.label_6 = QLabel(AddDialog)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_6.setWidget(0, QFormLayout.LabelRole, self.label_6)

        self.comboBox_3 = QComboBox(AddDialog)
        self.comboBox_3.setObjectName(u"comboBox_3")

        self.formLayout_6.setWidget(0, QFormLayout.FieldRole, self.comboBox_3)


        self.horizontalLayout_2.addLayout(self.formLayout_6)

        self.formLayout_7 = QFormLayout()
        self.formLayout_7.setObjectName(u"formLayout_7")
        self.label_7 = QLabel(AddDialog)
        self.label_7.setObjectName(u"label_7")

        self.formLayout_7.setWidget(0, QFormLayout.LabelRole, self.label_7)

        self.comboBox_4 = QComboBox(AddDialog)
        self.comboBox_4.setObjectName(u"comboBox_4")

        self.formLayout_7.setWidget(0, QFormLayout.FieldRole, self.comboBox_4)


        self.horizontalLayout_2.addLayout(self.formLayout_7)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.formLayout_8 = QFormLayout()
        self.formLayout_8.setObjectName(u"formLayout_8")
        self.label_8 = QLabel(AddDialog)
        self.label_8.setObjectName(u"label_8")

        self.formLayout_8.setWidget(0, QFormLayout.LabelRole, self.label_8)

        self.comboBox_5 = QComboBox(AddDialog)
        self.comboBox_5.setObjectName(u"comboBox_5")

        self.formLayout_8.setWidget(0, QFormLayout.FieldRole, self.comboBox_5)


        self.horizontalLayout_3.addLayout(self.formLayout_8)

        self.formLayout_9 = QFormLayout()
        self.formLayout_9.setObjectName(u"formLayout_9")
        self.label_9 = QLabel(AddDialog)
        self.label_9.setObjectName(u"label_9")

        self.formLayout_9.setWidget(0, QFormLayout.LabelRole, self.label_9)

        self.lineEdit_4 = QLineEdit(AddDialog)
        self.lineEdit_4.setObjectName(u"lineEdit_4")

        self.formLayout_9.setWidget(0, QFormLayout.FieldRole, self.lineEdit_4)


        self.horizontalLayout_3.addLayout(self.formLayout_9)

        self.formLayout_10 = QFormLayout()
        self.formLayout_10.setObjectName(u"formLayout_10")
        self.label_10 = QLabel(AddDialog)
        self.label_10.setObjectName(u"label_10")

        self.formLayout_10.setWidget(0, QFormLayout.LabelRole, self.label_10)

        self.lineEdit_5 = QLineEdit(AddDialog)
        self.lineEdit_5.setObjectName(u"lineEdit_5")

        self.formLayout_10.setWidget(0, QFormLayout.FieldRole, self.lineEdit_5)


        self.horizontalLayout_3.addLayout(self.formLayout_10)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.checkBox = QCheckBox(AddDialog)
        self.checkBox.setObjectName(u"checkBox")

        self.horizontalLayout_4.addWidget(self.checkBox)

        self.checkBox_2 = QCheckBox(AddDialog)
        self.checkBox_2.setObjectName(u"checkBox_2")

        self.horizontalLayout_4.addWidget(self.checkBox_2)

        self.formLayout_11 = QFormLayout()
        self.formLayout_11.setObjectName(u"formLayout_11")
        self.label_11 = QLabel(AddDialog)
        self.label_11.setObjectName(u"label_11")

        self.formLayout_11.setWidget(0, QFormLayout.LabelRole, self.label_11)

        self.comboBox_7 = QComboBox(AddDialog)
        self.comboBox_7.setObjectName(u"comboBox_7")

        self.formLayout_11.setWidget(0, QFormLayout.FieldRole, self.comboBox_7)


        self.horizontalLayout_4.addLayout(self.formLayout_11)


        self.verticalLayout.addLayout(self.horizontalLayout_4)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.widget = QWidget(AddDialog)
        self.widget.setObjectName(u"widget")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QSize(0, 60))
        self.widget.setMaximumSize(QSize(16777215, 60))
        self.gridLayout = QGridLayout(self.widget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer)

        self.pushButton_close = QPushButton(self.widget)
        self.pushButton_close.setObjectName(u"pushButton_close")

        self.horizontalLayout_5.addWidget(self.pushButton_close)

        self.pushButton_sure = QPushButton(self.widget)
        self.pushButton_sure.setObjectName(u"pushButton_sure")

        self.horizontalLayout_5.addWidget(self.pushButton_sure)


        self.gridLayout.addLayout(self.horizontalLayout_5, 0, 0, 1, 1)


        self.verticalLayout_2.addWidget(self.widget)


        self.gridLayout_2.addLayout(self.verticalLayout_2, 0, 0, 1, 1)


        self.retranslateUi(AddDialog)

        QMetaObject.connectSlotsByName(AddDialog)
    # setupUi

    def retranslateUi(self, AddDialog):
        AddDialog.setWindowTitle(QCoreApplication.translate("AddDialog", u"\u65b0\u589e\u8f6c\u6362", None))
        self.label.setText(QCoreApplication.translate("AddDialog", u"\u5f85\u7ffb\u8bd1\u89c6\u9891", None))
        self.pushButton_3.setText(QCoreApplication.translate("AddDialog", u"\u9009\u62e9", None))
        self.label_2.setText(QCoreApplication.translate("AddDialog", u"\u4fdd\u5b58\u76ee\u5f55", None))
        self.pushButton_4.setText(QCoreApplication.translate("AddDialog", u"\u9009\u62e9", None))
        self.label_3.setText(QCoreApplication.translate("AddDialog", u"\u9009\u62e9\u7ffb\u8bd1", None))
        self.label_4.setText(QCoreApplication.translate("AddDialog", u"\u4ee3\u7406\u5730\u5740", None))
        self.label_5.setText(QCoreApplication.translate("AddDialog", u"\u6e90\u8bed\u8a00", None))
        self.label_6.setText(QCoreApplication.translate("AddDialog", u"\u76ee\u6807\u8bed\u8a00", None))
        self.label_7.setText(QCoreApplication.translate("AddDialog", u"\u914d\u97f3\u89d2\u8272", None))
        self.label_8.setText(QCoreApplication.translate("AddDialog", u"\u8bed\u97f3\u8bc6\u522b\u6a21\u578b", None))
        self.label_9.setText(QCoreApplication.translate("AddDialog", u"\u8bed\u97f3\u914d\u901f", None))
        self.label_10.setText(QCoreApplication.translate("AddDialog", u"\u9759\u97f3\u7247\u6bb5", None))
        self.checkBox.setText(QCoreApplication.translate("AddDialog", u"CUDA\u52a0\u901f", None))
        self.checkBox_2.setText(QCoreApplication.translate("AddDialog", u"\u914d\u97f3\u81ea\u52a8\u52a0\u901f", None))
        self.label_11.setText(QCoreApplication.translate("AddDialog", u"\u5b57\u5e55", None))
        self.pushButton_close.setText(QCoreApplication.translate("AddDialog", u"\u53d6\u6d88", None))
        self.pushButton_sure.setText(QCoreApplication.translate("AddDialog", u"\u786e\u8ba4", None))
    # retranslateUi

