# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MainWindow.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from .resource_rc import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(900, 760)
        MainWindow.setMinimumSize(QSize(900, 760))
        self.verticalLayout_4 = QVBoxLayout(MainWindow)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.widget_menu = QWidget(MainWindow)
        self.widget_menu.setObjectName(u"widget_menu")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_menu.sizePolicy().hasHeightForWidth())
        self.widget_menu.setSizePolicy(sizePolicy)
        self.widget_menu.setMinimumSize(QSize(80, 0))
        self.widget_menu.setMaximumSize(QSize(80, 16777215))
        self.widget_menu.setStyleSheet(u"")
        self.gridLayout_4 = QGridLayout(self.widget_menu)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.widget_2 = QWidget(self.widget_menu)
        self.widget_2.setObjectName(u"widget_2")
        self.gridLayout_2 = QGridLayout(self.widget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_logo = QLabel(self.widget_2)
        self.label_logo.setObjectName(u"label_logo")
        font = QFont()
        font.setPointSize(10)
        self.label_logo.setFont(font)
        self.label_logo.setLayoutDirection(Qt.RightToLeft)
        self.label_logo.setStyleSheet(u"color: rgb(255, 255, 255);\n"
                                      "padding:10px;")

        self.gridLayout.addWidget(self.label_logo, 0, 0, 1, 1)

        self.pushButton_list = QPushButton(self.widget_2)
        self.pushButton_list.setObjectName(u"pushButton_list")
        self.pushButton_list.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_list.setAutoFillBackground(True)
        self.pushButton_list.setStyleSheet(u"")
        icon = QIcon()
        icon.addFile(u":/icons/assets/icons/list.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_list.setIcon(icon)
        self.pushButton_list.setFlat(True)

        self.gridLayout.addWidget(self.pushButton_list, 1, 0, 1, 1)

        self.pushButton_add = QPushButton(self.widget_2)
        self.pushButton_add.setObjectName(u"pushButton_add")
        self.pushButton_add.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_add.setAutoFillBackground(True)
        self.pushButton_add.setStyleSheet(u"")
        icon1 = QIcon()
        icon1.addFile(u":/icons/assets/icons/add.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_add.setIcon(icon1)
        self.pushButton_add.setFlat(True)

        self.gridLayout.addWidget(self.pushButton_add, 2, 0, 1, 1)

        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.verticalLayout_2.addWidget(self.widget_2)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.widget_3 = QWidget(self.widget_menu)
        self.widget_3.setObjectName(u"widget_3")
        self.gridLayout_3 = QGridLayout(self.widget_3)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.pushButton_setting = QPushButton(self.widget_3)
        self.pushButton_setting.setObjectName(u"pushButton_setting")
        self.pushButton_setting.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_setting.setAutoFillBackground(True)
        self.pushButton_setting.setStyleSheet(u"")
        icon2 = QIcon()
        icon2.addFile(u":/icons/assets/icons/set.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_setting.setIcon(icon2)
        self.pushButton_setting.setFlat(True)

        self.verticalLayout.addWidget(self.pushButton_setting)

        self.pushButton_help = QPushButton(self.widget_3)
        self.pushButton_help.setObjectName(u"pushButton_help")
        self.pushButton_help.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_help.setAutoFillBackground(True)
        self.pushButton_help.setStyleSheet(u"")
        icon3 = QIcon()
        icon3.addFile(u":/icons/assets/icons/help.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_help.setIcon(icon3)
        self.pushButton_help.setFlat(True)

        self.verticalLayout.addWidget(self.pushButton_help)

        self.gridLayout_3.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.verticalLayout_2.addWidget(self.widget_3)

        self.gridLayout_4.addLayout(self.verticalLayout_2, 0, 0, 1, 1)

        self.horizontalLayout.addWidget(self.widget_menu)

        self.widget_submenu = QWidget(MainWindow)
        self.widget_submenu.setObjectName(u"widget_submenu")
        sizePolicy.setHeightForWidth(self.widget_submenu.sizePolicy().hasHeightForWidth())
        self.widget_submenu.setSizePolicy(sizePolicy)
        self.widget_submenu.setMinimumSize(QSize(160, 0))
        self.widget_submenu.setMaximumSize(QSize(160, 16777215))
        self.widget_submenu.setStyleSheet(u"")
        self.gridLayout_5 = QGridLayout(self.widget_submenu)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.verticalLayout_submenu = QVBoxLayout()
        self.verticalLayout_submenu.setSpacing(0)
        self.verticalLayout_submenu.setObjectName(u"verticalLayout_submenu")
        self.verticalSpacer_2 = QSpacerItem(20, 20, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout_submenu.addItem(self.verticalSpacer_2)

        self.gridLayout_5.addLayout(self.verticalLayout_submenu, 0, 0, 1, 1)

        self.horizontalLayout.addWidget(self.widget_submenu)

        self.widget_content = QWidget(MainWindow)
        self.widget_content.setObjectName(u"widget_content")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widget_content.sizePolicy().hasHeightForWidth())
        self.widget_content.setSizePolicy(sizePolicy1)
        self.widget_content.setStyleSheet(u"")
        self.gridLayout_9 = QGridLayout(self.widget_content)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_9.setHorizontalSpacing(0)
        self.gridLayout_9.setContentsMargins(-1, 0, 0, 0)
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.widget_8 = QWidget(self.widget_content)
        self.widget_8.setObjectName(u"widget_8")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.widget_8.sizePolicy().hasHeightForWidth())
        self.widget_8.setSizePolicy(sizePolicy2)
        self.widget_8.setMinimumSize(QSize(0, 40))
        self.widget_8.setMaximumSize(QSize(16777215, 40))
        self.gridLayout_8 = QGridLayout(self.widget_8)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.pushButton_max = QPushButton(self.widget_8)
        self.pushButton_max.setObjectName(u"pushButton_max")
        icon4 = QIcon()
        icon4.addFile(u":/icons/assets/icons/maximize.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_max.setIcon(icon4)

        self.gridLayout_8.addWidget(self.pushButton_max, 0, 1, 1, 1)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.pushButton_min = QPushButton(self.widget_8)
        self.pushButton_min.setObjectName(u"pushButton_min")
        icon5 = QIcon()
        icon5.addFile(u":/icons/assets/icons/minus.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_min.setIcon(icon5)

        self.horizontalLayout_3.addWidget(self.pushButton_min)

        self.gridLayout_8.addLayout(self.horizontalLayout_3, 0, 0, 1, 1)

        self.pushButton_close = QPushButton(self.widget_8)
        self.pushButton_close.setObjectName(u"pushButton_close")
        icon6 = QIcon()
        icon6.addFile(u":/icons/assets/icons/close.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_close.setIcon(icon6)

        self.gridLayout_8.addWidget(self.pushButton_close, 0, 2, 1, 1)

        self.verticalLayout_3.addWidget(self.widget_8)

        self.widget_7 = QWidget(self.widget_content)
        self.widget_7.setObjectName(u"widget_7")

        self.verticalLayout_3.addWidget(self.widget_7)

        self.gridLayout_9.addLayout(self.verticalLayout_3, 0, 0, 1, 1)

        self.horizontalLayout.addWidget(self.widget_content)

        self.verticalLayout_4.addLayout(self.horizontalLayout)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)

    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Form", None))
        self.label_logo.setText(QCoreApplication.translate("MainWindow", u"LO", None))
        self.pushButton_list.setText("")
        self.pushButton_add.setText("")
        self.pushButton_setting.setText("")
        self.pushButton_help.setText("")
        self.pushButton_max.setText("")
        self.pushButton_min.setText("")
        self.pushButton_close.setText("")
    # retranslateUi
