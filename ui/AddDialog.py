import sys

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from .UI_AddDialog import Ui_AddDialog


class AddDialog(QDialog, Ui_AddDialog):
    def __init__(self, parent=None):
        super(AddDialog, self).__init__(parent)
        self.setupUi(self)

        self.initWidget()

    def initWidget(self):
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.pushButton_close.clicked.connect(self.cancel)

    def cancel(self):
        self.close()
